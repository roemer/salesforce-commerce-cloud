'use strict'

var logger = dw.system.Logger.getLogger("Spryng_Payments");

/**
 * Removes existing payment instruments from an order.
 * 
 * @param basket dw.order.Basket
 * @returns boolean|null
 */
var RemovePaymentInstruments = {
		basket: null,
		
		init: function(basket) {
			this.basket = basket;
			
			return this;
		},
		
		remove: function() {
			if (typeof this.basket === 'undefined' || empty(this.basket)) {
				logger.debug('Basket was empty. Exiting.');
				return false;
			}

			var piCollection = this.basket.getPaymentInstruments();
			var piIterator = piCollection.iterator();
			var existingPI = null;

			while (piIterator.hasNext())
			{
				existingPI = piIterator.next();
				basket.removePaymentInstrument(existingPI);
			}

			return true;
		}
}

module.exports = RemovePaymentInstruments