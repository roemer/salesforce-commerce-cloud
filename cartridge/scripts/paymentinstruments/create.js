'use strict'

var logger = dw.system.Logger.getLogger("Spryng_Payments");
var Money = require('dw/value/Money');

var CreatePaymentInstrument = {
		basket: null,
		
		init: function(basket) {
			this.basket = basket;
			
			return this;
		},
		
		create: function() {
			var amount = this.calculateNonGiftCertificateAmount(this.basket);
			var moneyAmount = new Money(amount, this.basket.currencyCode);
			var instrument = this.basket.createPaymentInstrument("Spryng_Payments", moneyAmount);
			
			return instrument;
		},
		
		calculateNonGiftCertificateAmount: function() {
			var totalAmount = this.basket.totalGrossPrice;
			var	giftAmount = 0.0;
			
			var giftCertCollection = this.basket.getGiftCertificatePaymentInstruments();
			var iterator = giftCertCollection.iterator();
			var existing = null;
			
			while (iterator.hasNext()) {
				var existing = iterator.next();
				giftAmount = giftAmount + existing.getPaymentTransaction().getAmount();
			}
			
			totalAmount = totalAmount - giftAmount;
			return totalAmount;
		}
}

module.exports = CreatePaymentInstrument;