'use strict'

var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var logger	= dw.system.Logger.getLogger("Spryng_Payments");

function handle(args) {
	/* Script modules */
	var removeInstruments = require('~/cartridge/scripts/paymentinstruments/remove').init(args.basket);
	var createInstruments = require('~/cartridge/scripts/paymentinstruments/create').init(args.basket);
	
	var result;
	
	Transaction.wrap(function() {
		result = removeInstruments.remove();
		if (!result) {
			return {error: true};
		}
		
		result = createInstruments.create();
	});
	
	if (result != null) {
		logger.debug("Successfully created Spryng Payments payment instrument");
		return {success: true}
	}
	
	logger.debug("Error occured while creating Spryng Payments payment instrument");
	return {error: true}
}

function authorize(args) {
	logger.debug("Hi from the authorize function!");
}

exports.Handle = handle;
exports.Authorize = authorize;