/**
* Description of the Controller and the logic it provides
*
* @module  controllers/Spryng_Payments
*/

'use strict';

var logger = require('dw/system/Logger').getLogger('Spryng Payments', 'spryngpayments');

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
function handle() {
	logger.debug('It has been triggered.');
}

exports.Handle = handle;